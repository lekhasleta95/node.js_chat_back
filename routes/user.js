const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');

//User model
const User = require('../models/User');

//Register handle
router.post('/register', (req, res) => {
    const { name, email, password, password2 } = req.body;
    let errors = [];

    //Check required fields
    if (!name || !email || !password || !password2) {
        errors.push({ msg: 'Please fill in all fields' })
    }

    //Check passwords match
    if (password !== password2) {
        errors.push({ msg: 'Passwords do not match' });
    }

    //Check pass length
    if (password.length < 6) {
        errors.push({ msg: 'Password should be at least 6 character' });
    }
    if (errors.length > 0) {
        res.send({ errors: errors });
    } else {
        //Valid passed
        User.findOne({ email: email }).then(user => {
            if (user) {
                //User exists
                errors.push({ msg: 'Email is already register' });
                res.send({ errors: errors });
            } else {
                const newUser = new User({
                    name,
                    email,
                    password
                });
                //Hash Pasword
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if (err) throw err;
                        //Set pass to hash
                        newUser.password = hash;
                        //Save user
                        newUser.save().then(user => {
                            res.send('User saved!');
                        }).catch(err => console.log(err)
                        )
                    })
                })
            }
        }).catch(err => {
            console.log(err);
        })
    }
});

//Login Handle

router.post('/login', passport.authenticate('local',

    { failureFlash: true }),

    (req, res) => {

        // If this function gets called, authentication was successful.
        // `req.user` contains the authenticated user.
        res.send({
            _id: req.user._id,
            name: req.user.name,
            email: req.user.email,
        });

    });

//Logout handle

router.get('/logout', (req, res) => {
    req.logout();
    res.send('Logout success!')
})

module.exports = router;