const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');

const app = express();


//Passport config 
require('./config/passport')(passport)

app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

// DB config
const urldb = "mongodb://localhost:27017/chat";

//Connect to MongoDB
mongoose.connect(urldb, { useNewUrlParser: true }).then(() => {
    console.log('MongoDB Connected...');
}).catch(err => {
    console.log(err);
});

//Bodyparser
app.use(express.urlencoded({ extended: false }));

//Express session midleware
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
}));

//Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

//Routes
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/user'));


const server = app.listen(5000, function () {
    console.log('server running on port 5000');
});

const io = require('socket.io')(server);
const Message = require('./models/Message');


io.on('connection', function (socket) {

    let messages = [];

    Message.find().limit(50).then(mess => {
        messages = mess;
        io.emit("ALL_MESSAGES", messages);
    })

    

    socket.on('SEND_MESSAGE', function (data) {

        const newMessage = new Message({
            _id: new mongoose.Types.ObjectId(),
            user: data.user,
            message: data.message
        });

        newMessage.save().then(mess => {
            console.log('Message is saved');
        }).catch(err => {
            console.log(err);
        })

        io.emit('MESSAGE', data)
    });

});