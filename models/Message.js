const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    user: {
        type: Object, 
        required: true
    },
    message: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
});
const Message = mongoose.model('Message', MessageSchema);

module.exports = Message;